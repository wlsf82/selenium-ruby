require "rspec"
require "selenium-webdriver"

include RSpec::Matchers

[:chrome, :firefox, :safari].each do |browser|
  driver = Selenium::WebDriver.for browser

  walmyr_filho_web_site_url = 'https://walmyr-filho.com/'
    
  driver.manage.window.maximize
  driver.get walmyr_filho_web_site_url
  driver.navigate.to 'https://talkingabouttesting.com'
  driver.navigate.back
    
  expect(driver.current_url).to eq walmyr_filho_web_site_url
  expect(driver.find_element(:css, 'h1').text.upcase).to eq 'WALMYR LIMA E SILVA FILHO'
    
  driver.quit
end