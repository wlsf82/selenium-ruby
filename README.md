# Learning from studies of Selenium using Ruby

## Pre-requirements

- Ruby installed (v2.5.3)
- Bundle installed

## Rake tasks

Run `rake` to run the tests in the following browsers: Chrome, Firefox, and Safari.

## Command line

Following is a series of commands to interact with the browser through the command line, separated per browser.

### Chrome

```
$ irb
2.5.3 :001 > require "selenium-webdriver"
 => true
2.5.3 :002 > driver = Selenium::WebDriver.for :chrome
 => #<Selenium::WebDriver::Safari::Driver:0x7bdedcde1888cff2 browser=:Safari>
2.5.3 :003 > driver.manage.window.maximize
 => {"y"=>23, "x"=>0, "width"=>1440, "height"=>877}
2.5.3 :005 > driver.get 'http://walmyr-filho.com'
 => nil
2.5.3 :006 > driver.navigate.to 'https://talkingabouttesting.com'
 => nil
2.5.3 :007 > driver.navigate.back
 => nil
2.5.3 :008 > driver.current_url
 => "https://walmyr-filho.com/"
2.5.3 :009 > driver.find_element(:css, 'h1').text
 => "Walmyr Lima e Silva Filho"
2.5.3 :011 > driver.quit
 => nil
2.5.3 :012 >
```

### Firefox

```
$ irb
2.5.3 :001 > require "selenium-webdriver"
 => true
2.5.3 :002 > driver = Selenium::WebDriver.for :firefox
 => #<Selenium::WebDriver::Safari::Driver:0x7bdedcde1888cff2 browser=:Safari>
2.5.3 :003 > driver.manage.window.maximize
 => {"y"=>23, "x"=>0, "width"=>1440, "height"=>877}
2.5.3 :005 > driver.get 'http://walmyr-filho.com'
 => nil
2.5.3 :006 > driver.navigate.to 'https://talkingabouttesting.com'
 => nil
2.5.3 :007 > driver.navigate.back
 => nil
2.5.3 :008 > driver.current_url
 => "https://walmyr-filho.com/"
2.5.3 :009 > driver.find_element(:css, 'h1').text
 => "Walmyr Lima e Silva Filho"
2.5.3 :011 > driver.quit
 => nil
2.5.3 :012 >
```

### Safari

1. Configure Safari to show the Develop menu in the menu bar (Safari | Preferences | Advanced | Show Develop menu in the menu bar).
2. Allow Safari remote automation (Develop | Allow Remote Automation).

```
$ irb
2.5.3 :001 > require "selenium-webdriver"
 => true
2.5.3 :002 > driver = Selenium::WebDriver.for :safari
 => #<Selenium::WebDriver::Safari::Driver:0x7bdedcde1888cff2 browser=:Safari>
2.5.3 :003 > driver.manage.window.maximize
 => {"y"=>23, "x"=>0, "width"=>1440, "height"=>877}
2.5.3 :005 > driver.get 'http://walmyr-filho.com'
 => nil
2.5.3 :006 > driver.navigate.to 'https://talkingabouttesting.com'
 => nil
2.5.3 :007 > driver.navigate.back
 => nil
2.5.3 :008 > driver.current_url
 => "https://walmyr-filho.com/"
2.5.3 :009 > driver.find_element(:css, 'h1').text
 => "Walmyr Lima e Silva Filho"
2.5.3 :011 > driver.quit
 => nil
2.5.3 :012 >
```
